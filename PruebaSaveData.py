import pickle
import os.path as path

class pj:
	def __init__(self,nombre):
		self.nombre = nombre
		self.oro = 0
		self.exp = 0
		info = [self.oro, self.exp]
		
		if path.exists('SaveData.pckl'):
			fichero = open('SaveData.pckl','rb') #rb lectura binaria
			lista = pickle.load(fichero)
			self.oro = lista[0]	
			self.exp = lista[1]			
		else:
			fichero = open('SaveData.pckl','wb') #wb Escritura binaria
			pickle.dump(info, fichero)
			fichero.close()

	def ganarOro(self,oro):
		self.oro += oro
		self.guardarInfo()

	def ganarExp(self,exp):
		self.exp += exp
		self.guardarInfo()

	def guardarInfo(self):
		info = [self.oro, self.exp]
		fichero = open('SaveData.pckl','wb')
		pickle.dump(info, fichero)
		fichero.close()	

	def verInfo(self):
		fichero = open('SaveData.pckl','rb')
		lista = pickle.load(fichero)
		fichero.close()
		print(lista)

a = pj("Holi")
a.verInfo()
a.ganarExp(123)
a.ganarOro(18)
a.verInfo()